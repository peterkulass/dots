import { useState } from "react";
import "./App.css";
import { Box, Button } from "@mui/material";

function App() {
  const [dots, setDots] = useState([]);
  const [undoArray, setUndoArray] = useState([]);

  function getRandomColor() {
    const randomR = Math.floor(Math.random() * 256);
    const randomG = Math.floor(Math.random() * 256);
    const randomB = Math.floor(Math.random() * 256);
    return `rgb(${randomR}, ${randomG}, ${randomB})`;
  }

  const createDot = (e) => {
    const dot = { x: e.pageX - 5, y: e.pageY - 5, color: getRandomColor() };
    setDots([...dots, dot]);
    console.log(dot.x, dot.y);
  };

  const handleUndo = () => {
    const undoDot = dots.pop();
    setUndoArray([...undoArray, undoDot]);
  };

  const handleRedo = () => {
    const redoDot = undoArray.pop();
    setDots([...dots, redoDot]);
  };

  return (
    <Box justifyContent="center">
      <Button
        disabled={dots.length === 0}
        variant="contained"
        sx={{ mt: 2, mr: 2, zIndex: 999 }}
        onClick={handleUndo}>
        Undo
      </Button>
      <Button
        disabled={undoArray.length === 0}
        variant="contained"
        sx={{ mt: 2, zIndex: 999 }}
        onClick={handleRedo}>
        Redo
      </Button>
      <div className="App" onClick={createDot}>
        {dots.map((item, idx) => (
          <div
            className="dot"
            key={idx}
            style={{ top: item.y, left: item.x, backgroundColor: item.color }}
          />
        ))}
      </div>
    </Box>
  );
}

export default App;
